#pragma once
#include <string>

class Gene {

public:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;

	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	
	unsigned int get_start() const;
	void set_start(const unsigned int start);
	
	unsigned int get_end() const;
	void set_end(const unsigned int end);

	bool is_on_complementary_dna_strand() const;
	void set_on_complementary_dna_strand(const unsigned int on_complementary_dna_strand);

};

class Nucleus {

public:
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;

	void init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;

};
