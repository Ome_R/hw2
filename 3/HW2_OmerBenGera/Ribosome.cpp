#include "Ribosome.h"

using std::string;

Protein* Ribosome::create_protein(string &RNA_transcript) const {
	Protein* protein = new Protein;
	protein->init();

	while (RNA_transcript.length() >= 3) {
		string codon = RNA_transcript.substr(0, 3);
		RNA_transcript = RNA_transcript.substr(3);
		AminoAcid aminoAcid = get_amino_acid(codon);

		if (aminoAcid == UNKNOWN) {
			delete(protein);
			return nullptr;
		}
		else {
			protein->add(aminoAcid);
		}
	}

	return protein;
}