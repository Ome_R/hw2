#include <iostream>
#include "Nucleus.h"

using std::string;
using std::cerr;
using std::endl;

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand) {
	_start = start;
	_end = end;
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

unsigned int Gene::get_start() const {
	return _start;
}

void Gene::set_start(const unsigned int start) {
	_start = start;
}

unsigned int Gene::get_end() const {
	return _end;
}

void Gene::set_end(const unsigned int end) {
	_end = end;
}

bool Gene::is_on_complementary_dna_strand() const {
	return _on_complementary_dna_strand;
}

void Gene::set_on_complementary_dna_strand(const unsigned int on_complementary_dna_strand) {
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

void Nucleus::init(const std::string dna_sequence) {
	unsigned int i = 0;
	_DNA_strand = dna_sequence;

	// Looping through all the chars
	for (i = 0; i < dna_sequence.length(); i++) {
		switch (dna_sequence[i]) {
			case 'A':
				_complementary_DNA_strand += 'T';
				break;
			case 'G':
				_complementary_DNA_strand += 'C';
				break;
			case 'C':
				_complementary_DNA_strand += 'G';
				break;
			case 'T':
				_complementary_DNA_strand += 'A';
				break;
			// Error with DNA
			default:
				cerr << "There's an error with Nucleus init - Found an unknown dna char." << endl;
				_exit(1);
				break;
		}
	}

}

string Nucleus::get_RNA_transcript(const Gene& gene) const {
	unsigned int i = 0;
	string transcript = "";
	string dna = gene.is_on_complementary_dna_strand() ? _complementary_DNA_strand : _DNA_strand;

	for (i = gene.get_start(); i <= gene.get_end(); i++) {
		transcript += dna[i] == 'T' ? 'U' : dna[i];
	}

	return transcript;
}

string Nucleus::get_reversed_DNA_strand() const {
	string dna = _DNA_strand;
	dna.reserve();
	return dna;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const {
	unsigned int count = 0;
	int pos = 0 - codon.length();

	while ((pos = _DNA_strand.find(codon, pos + codon.length())) != -1)
		count++;

	return count;
}
