#include "Cell.h"

using std::cout;
using std::endl;
using std::string;

void Cell::init(const string dna_sequence, const Gene glucose_receptor_gene) {
	_nucleus.init(dna_sequence);
	_mitochondrion.init();
	_glocus_receptor_gene = glucose_receptor_gene;
}

bool Cell::get_ATP() {
	string transcript = _nucleus.get_RNA_transcript(_glocus_receptor_gene);
	Protein* protein = _ribosome.create_protein(transcript);

	if (protein == nullptr) {
		cout << "Couldn't create a protein from trasncript." << endl;
		_exit(1);
	}

	_mitochondrion.insert_glucose_receptor(*protein);

	if (_mitochondrion.produceATP()) {
		_atp_units = 100;
		return true;
	}

	return false;
}